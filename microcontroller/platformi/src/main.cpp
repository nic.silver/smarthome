#include <Arduino.h>
#define LED 2

void setup() {
  // put your setup code here, to run once:
  pinMode(LED,OUTPUT);
  Serial.begin(9600);
}

void loop() {
  delay(250);
  digitalWrite(LED,HIGH);
  delay(250);
  digitalWrite(LED, LOW);
  Serial.println("Test");
  // put your main code here, to run repeatedly:
}